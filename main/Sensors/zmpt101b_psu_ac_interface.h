/* 
The MIT License (MIT)

Copyright (c) 2020 ANUJ.J (anujfalcon8@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or 
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT 
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#pragma once
#include "Sensors/zmpt101b.h"

void zmpt101b_routine(void *args){
    zmpt101b_struct *input = (zmpt101b_struct*) args;
    xTaskHandle *display_task = input->display_task;
    ESP_LOGI(ZMPT101B_TAG,"lol %d",input->max_V);
   
    uint32_t max_v = 0;
    uint32_t min_v = -1;
    uint32_t mid_v = 0;
    uint16_t idx = 0;
    uint16_t idx_ref_crossing = 0;
    uint16_t idx_diff_crossing = 0;
    bool capture_crossing_pos = false;
    
    // // To measure the sampling rate with external Oscope at GPIO 17   
    // gpio_set_direction(GPIO_NUM_17,GPIO_MODE_OUTPUT);
    // gpio_set_level(GPIO_NUM_17,1);

    TickType_t time_ref = xTaskGetTickCount();
    const TickType_t period = 1000/portTICK_PERIOD_MS;
    while(1)
    {
        uint32_t v = avg_measure_zmpt101b(20);
        
        if (v>max_v) max_v = v;
        else if (v<min_v) min_v = v;

        if(capture_crossing_pos && v >= mid_v) {
            capture_crossing_pos = false;
            idx_diff_crossing = idx - idx_ref_crossing;
            idx_ref_crossing = idx;
        } else if (!capture_crossing_pos && v < mid_v) capture_crossing_pos = true;
       
        // // To measure the sampling rate with external Oscope at GPIO 17   
        // if(idx%2) gpio_set_level(GPIO_NUM_17,0);
        // else gpio_set_level(GPIO_NUM_17,1);
       
        idx++;
        if(idx == SAMPLES_PER_READING) {
            input->max_V = max_v;
            input->min_V = min_v;
            input->idx_crosses_NP = idx_diff_crossing; 
            mid_v = (max_v + min_v) / 2;

            bool sent = false;
            while(!sent) {
                if(xTaskNotify(*display_task,0,eSetValueWithoutOverwrite)){
                    if(xTaskNotifyWait(0,0,NULL,100/portTICK_PERIOD_MS))
                        sent = true;
                }
                else vTaskDelay(100/portTICK_PERIOD_MS);
            }
            ESP_LOGI(ZMPT101B_TAG,"Max V = %d",max_v);
            ESP_LOGI(ZMPT101B_TAG,"Min V = %d",min_v);
            ESP_LOGI(ZMPT101B_TAG,"Pos. Crosses = %d",idx_diff_crossing);

            max_v = 0;
            min_v = -1;
            idx = 0;
            
            vTaskDelayUntil(&time_ref,period);
        }
    }
}