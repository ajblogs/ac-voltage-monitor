/* 
The MIT License (MIT)

Copyright (c) 2020 ANUJ.J (anujfalcon8@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or 
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT 
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "Sensors/zmpt101b.h"
#include "Sensors/zmpt101b_psu_ac_interface.h"

//Useless define as the ADC uses eFuse Vref fused during factory calibration as ADC reference voltage
//for my chip.
#define DEFAULT_VREF 1111

static const char* ACV = "ACV Sense";

static esp_adc_cal_characteristics_t *adc_chars;
void config_zmpt101b(){
    ESP_LOGI(ACV,"Starting adc...");
    adc1_config_width(ADC_WIDTH);
    adc1_config_channel_atten(ADC_CHANNEL, ADC_ATTEN);
    // Characterizing ADC. This additional step needed due to poor linearity
    adc_chars = calloc(1,sizeof(esp_adc_cal_characteristics_t));
    esp_adc_cal_characterize(ADC_UNIT, ADC_ATTEN, ADC_WIDTH, DEFAULT_VREF, adc_chars);
}

//Output the average voltage measured from the given size of samples
uint32_t avg_measure_zmpt101b(uint8_t samples){
    uint32_t volt = 0;
    for(uint8_t i = 0; i<samples; i++){
        volt += adc1_get_raw(ADC_CHANNEL);
    }
    volt /= samples;
    volt = esp_adc_cal_raw_to_voltage(volt, adc_chars);
    // ESP_LOGI(ACV,"Avg voltage: %d mv", volt);
    return (volt - ADC_DC_OFFSET_mV);
}

//Ouptut the raw ADC value without voltage converstion
uint32_t raw_measure_zmpt101b(){
    uint32_t volt = adc1_get_raw(ADC_CHANNEL);
    // ESP_LOGI(ACV,"Raw voltage: %d LSBs", volt);
    return (volt - ADC_DC_OFFSET_mV);
}

//Ouptut the Voltage value
uint32_t measure_zmpt101b(){
    uint32_t volt;
    ESP_ERROR_CHECK(esp_adc_cal_get_voltage(ADC_CHANNEL, adc_chars, &volt));
    ESP_LOGI(ACV,"Voltage: %d mv", volt);
    return (volt - ADC_DC_OFFSET_mV);
}