
/* 
The MIT License (MIT)

Copyright (c) 2020 ANUJ.J (anujfalcon8@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or 
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT 
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "system_definitions.h"
#include "driver/gpio.h"
#include "Actuators/st7789v.h"
#include "Sensors/zmpt101b.h"

static const char* SYSTEM = "SYSTEM";

void app_main()
{
    //Setup zmpt101b and ADC
    ESP_LOGI(SYSTEM,"Initiating!");
    config_zmpt101b();

    //Setup display and the SPI
    lcd_struct lcd = {0xffffff,spi_setup()};
    font_struct font32 = {chr_hgt_f32,max_width_f32,firstchr_f32,widtbl_f32,chrtbl_f32,font_rle_decoder,0x0,-1,&lcd};
    lcd_fill(lcd.background ,&lcd);
    display_logo(&font32);

    graph_struct gset_rms = {66,PIX_WIDTH,1,0,0xEEEEEE,-1,0,40,&lcd};
    xTaskHandle zmpt101b_handle, st7789v_handle;
    zmpt101b_struct zmpt101b_input = {}; zmpt101b_input.display_task = &st7789v_handle;
    st7789v_struct st7789v_input = {&zmpt101b_input, &lcd, &gset_rms, &zmpt101b_handle};

    xTaskCreate(st7789v_routine, "display task", 3072, (void*) &st7789v_input, 10, &st7789v_handle);
    xTaskCreate(zmpt101b_routine, "AC Volt sense", 2048, (void*) &zmpt101b_input, 10, &zmpt101b_handle);
    
    while(1){
        vTaskDelay(portMAX_DELAY);
    }
}