/* 
The MIT License (MIT)

Copyright (c) 2020 ANUJ.J (anujfalcon8@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or 
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT 
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_log.h"
#include "sdkconfig.h"

/*ST7789V*/
/*Setup SPI interface for INA219*/
// #define PIN_NUM_MISO 25
#define PIN_NUM_MOSI 19
#define PIN_NUM_CLK  18
#define PIN_NUM_CS   5
#define PIN_NUM_DC   16
#define PIN_NUM_RST  23
#define PIN_NUM_BCKL 4
/*Custom message notification values*/
#define ST7789V_NOTIFY_RECEIVED 0xA0
#define ST7789V_NOTIFY_BUSY 0xA1
#define ST7789V_NOTIFY_AVAIL 0xA2
#define ST7789V_NOTIFY_NEW_MAX_A 0xA3
#define ST7789V_NOTIFY_NEW_MAX_V 0xA4
#define ST7789V_NOTIFY_NEW_MAX_W 0xA5
#define alert_default_display_timeperiod (6000/portTICK_PERIOD_MS)

//ZMP config
#define ADC_DC_OFFSET_mV 100
#define ZMPT101B_PIN ADC1_GPIO36_CHANNEL
#define ADC_CHANNEL ZMPT101B_PIN 
#define ADC_UNIT ADC_UNIT_1
#define ADC_ATTEN ADC_ATTEN_DB_11
#define ADC_WIDTH ADC_WIDTH_BIT_12
#define AC0V_INPUT_mV 1555
#define square_root_2 1.4142
#define SAMPLES_PER_READING 500

#define MAX_RMS_TO_GRAPH 30

//Only used as reference, as set by the user for frequency calculation.
#define SAMPLING_PERIOD_MS 0.87108
#define ZMPT101B_CONV_FACTOR 10
