/* 
The MIT License (MIT)

Copyright (c) 2020 ANUJ.J (anujfalcon8@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or 
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT 
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#pragma once
#include "Actuators/st7789v.h"

#define X_PIX_STATIC 130 // Value based on observation
#define Y_PIX_STATIC 28 // Value based on observation

/*Function to display Voltage,Current and Power*/
void display_logo(font_struct *font){
    char *container = "Anuj Falcon !";
    int u = lcd_text(5,Y_PIX_STATIC+10,container,font);
    vTaskDelay(1200/portTICK_PERIOD_MS);
    lcd_rect(font->lcd->background, 5, Y_PIX_STATIC+10, u-5,32,font->lcd);
}

/*Algorithm to plot two values with their respective color. Values are chosen based on the bool val*/
//GRAPH SECTION
static int _x = 0;
void graph_plot(int val, graph_struct *graph_settings){

    int sheet_height = graph_settings->sheet_height;
    int plot_width = graph_settings->plot_width;
    int sheet_width = graph_settings->sheet_width;
    
    color graph_rgb16 = rgb24_16(graph_settings->plot_color);
    color graph_back_rgb16 = rgb24_16(graph_settings->back_color);
    color graph_cursor_rgb16 = rgb24_16(graph_settings->cursor_color);

    uint16_t *dd = heap_caps_malloc(sheet_height*sizeof(uint16_t),MALLOC_CAP_DMA);
    assert(dd != NULL);

    // See if the val has acceptable within Y range
    assert(sheet_height >= val);
    
    for(int i=0;i<sheet_height;i++){
        if(sheet_height-val-1 == i) *(dd+i) = graph_rgb16;
        else *(dd+i) = graph_back_rgb16;          
    }
    
    send_rect((_x+graph_settings->x_offset),graph_settings->y_offset,plot_width,sheet_height,graph_settings->lcd->spi,dd);        
    _x++;
    if(_x>(sheet_width-1)) _x = 0;
    
    send_finish(graph_settings->lcd->spi);
    
    //Cursor
    for(int i=0;i<sheet_height;i++){
        *(dd+i) = graph_cursor_rgb16;          
    }
    send_rect((_x+graph_settings->x_offset),graph_settings->y_offset,plot_width,sheet_height,graph_settings->lcd->spi,dd);        
    send_finish(graph_settings->lcd->spi);
    heap_caps_free(dd);
}

void st7789v_routine(void *args){
    st7789v_struct* input = (st7789v_struct*) args;
    xTaskHandle *zmpt101b_task = input->zmpt101b_task;
    graph_struct *gset_rms = input->graph_settings;
    lcd_struct *lcd = input->lcd;

    uint32_t max_v = 0;
    uint32_t min_v = 0;
    uint16_t idx_diff_crossing = 0;
    font_struct font32 = {chr_hgt_f32,max_width_f32,firstchr_f32,widtbl_f32,chrtbl_f32,font_rle_decoder,0x0,-1,lcd};
    font_struct font16 = {chr_hgt_f16,max_width_f16,firstchr_f16,widtbl_f16,chrtbl_f16,font16_decoder,0x0,-1,lcd};
    char c[20];

    while(1){
        if(xTaskNotifyWait(0,0,NULL,portMAX_DELAY)){
            max_v = input->zmpt101b_data->max_V;
            min_v = input->zmpt101b_data->min_V;
            idx_diff_crossing = input->zmpt101b_data->idx_crosses_NP;
            xTaskNotify(*zmpt101b_task,0,eSetValueWithoutOverwrite);

            float freq = idx_diff_crossing*SAMPLING_PERIOD_MS;
            freq = (float)1000/freq;
            uint32_t avg_v = (max_v+min_v)/2;
            
            uint32_t PP = max_v - min_v;            
            float rms_v = (float)PP/(float)ZMPT101B_CONV_FACTOR;
            
            ESP_LOGI(ST7789V_TAG,"%05.1f Vrms\n",rms_v);
            ESP_LOGI(ST7789V_TAG,"Freq = %02.1f", freq);
            ESP_LOGI(ST7789V_TAG,"Max V %d",max_v);
            ESP_LOGI(ST7789V_TAG,"Min V %d",min_v);
            ESP_LOGI(ST7789V_TAG,"Avg V %d",avg_v);

            // 0x042D52
            sprintf(c,"%05.1f V",rms_v);
            lcd_rect(0xFF9900,8,-1,87,38,lcd);

            font32.font_back_rgb24 = 0xFF9900;
            font32.font_rgb24 = -1;
            int lend = lcd_text(10,10,c,&font32);

            lcd_text(lend+5,-2,"Sine",&font16);
            lend = lcd_text(lend+5,16,"RMS",&font16);

            font16.font_rgb24 = 0x00CC00 ;
            sprintf(c,"%04d",max_v);
            lcd_text(lend+7,-2,c,&font16);
            sprintf(c,"%04d",min_v);
            lend = lcd_text(lend+7,16,c,&font16);

            font16.font_rgb24 = 0;
            lcd_text(lend,-2,"mx",&font16);
            lend = lcd_text(lend,16,"mn",&font16);

            font16.font_rgb24 = 0;
            lcd_text(lend+6,-2,"AVG mV",&font16);
            sprintf(c,"%04d",avg_v);
            font16.font_rgb24 = 0x00CC00 ;
            lcd_text(lend+10,16,c,&font16);
            font16.font_rgb24 = 0;

            font32.font_back_rgb24 = -1;
            font32.font_rgb24 = 0xcc0000 ;
            sprintf(c,"Sine freq : ");
            lend = lcd_text(10,107,c,&font32);

            lcd_rect(0xFF9900,lend-2,107,102,33,lcd);
            font32.font_back_rgb24 = 0xFF9900;
            font32.font_rgb24 = -1 ;
            sprintf(c,"%05.1f Hz",freq);
            lcd_text(lend,107,c,&font32);
 
            if(rms_v < MAX_RMS_TO_GRAPH) {
                rms_v *= gset_rms->sheet_height;
                int val = rms_v / MAX_RMS_TO_GRAPH;
                graph_plot(val,gset_rms);
            }
        }
    }
}